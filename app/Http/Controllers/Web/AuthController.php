<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest'])->except('logout');
    }

    public function login(){
        return view('user.login');
    }
    
    public function loginStore(Request $request){
        $this->validate($request, [
            'email' => 'required|max:255|email',
            'password' => 'required'
        ]);
        if (!auth()->attempt($request->only('email', 'password'), $request->remember)) {
            Alert::error('Error', 'Invalid Login Details');
            return redirect()->back()->with('status', 'Invalid Login Details');
        }
        
        
        if(auth()->user()->isAdmin()){
            Alert::success('Success', 'Login as Admin Success');
            return redirect()->route('admin');
        }
        
        
        Alert::success('Success', 'Login Success');
        return redirect()->route('landingWeb')->with('success', 'Login Success');
    }
    
    
    public function register(){
        return view('user.register');
    }
    
    public function registerStore(Request $request){
        $this->validate($request, [
            'email' => 'required|max:255|email|exists:mysql.users,email',
            'name' => 'required|max:255',
            'password' => 'required|confirmed'
        ]);
        
        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => Hash::make($request->password)
        ]);
        
        auth()->attempt($request->only('email', 'password'));

        Alert::success('Success', 'Register Success');
        
        return redirect()->route('login')->with('success', 'Register Success');
    }
    
    public function logout(){
        auth()->logout();
        Alert::success('Success', 'Logout Success');
        return redirect()->route('landingWeb')->with('success', 'Logout Success');
    }
}
