# Final Project Laravel PKS DIGISCHOOL
### Kelompok  25
### Anggota Kelompok:
* Muhammad Kemal
* Farhan Dewanta Syahputra
* Wahyu Gunawan

### Tema Project 
###### E-Commerce (penjualan tanaman) : Naturalisme 

### Entity Relationship Diagram

![ERD Picture](erd.jpg)

### Link Deploy
[Link Deploy](https://naturalisme.herokuapp.com/)

### Link Video
[Link Video Demo](https://youtu.be/KKq8kHuqJYM)
