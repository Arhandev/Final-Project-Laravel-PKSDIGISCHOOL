<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name'=> "indoor",
        ]);
        Category::create([
            'name'=> "outdoor",
        ]);
        Category::create([
            'name'=> "perlengkapan",
        ]);
        Category::create([
            'name'=> "pupuk",
        ]);
    }
}
