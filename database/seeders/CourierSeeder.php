<?php

namespace Database\Seeders;

use App\Models\Courier;
use Illuminate\Database\Seeder;

class CourierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Courier::create([
            "name" => "JNE"
        ]);
        Courier::create([
            "name" => "TIKI"
        ]);
        Courier::create([
            "name" => "JNT"
        ]);
        Courier::create([
            "name" => "SICEPAT"
        ]);
    }
}
